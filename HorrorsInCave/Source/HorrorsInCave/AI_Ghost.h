// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AI_Enemy.h"
#include "AI_Ghost.generated.h"

/**
 * 
 */
UCLASS()
class HORRORSINCAVE_API AAI_Ghost : public AAI_Enemy
{
	GENERATED_BODY()
	
private:

	// set to true if the ghost is under the drone light
	bool bIsStunned;
	// If the ghost is under the drone light
	bool bIsUnderLight;

protected:


public:


	// Max health points for ghost
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character)
	float MAX_HEALTH = 3.f;

	// constructor
	AAI_Ghost();

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;


	/**
	*Set the stunned state
	*
	*@param NewStunnedState Set t for stunned and f for not stunned
	*/
	UFUNCTION(BlueprintCallable, Category = "Ghost")
	void SetStunned(bool NewStunnedState);


	/**
	*Get current state if it is stunned or not
	*
	*@return Return true if enemy is stunned, false if it is not stunned
	*/
	UFUNCTION(BlueprintPure, Category = "Ghost")
	bool IsStunned();


	/**
	*log that enemy was hit and destory enemy if it is stunned and active
	*
	*@param Other this is the other actor we check if it is a projectile then we see if we can destroy the enemy
	*/
	UFUNCTION(BlueprintCallable, Category = "Ghost")
	virtual void ReceiveHit(class UPrimitiveComponent *MyComp, class AActor *Other,
							class UPrimitiveComponent *OtherComp, bool bSelfMoved,
								FVector HitLocation, FVector HitNormal,
								FVector NormalImpulse, const FHitResult &Hit);

	/**
	*@Description Handles is the ghost light detection
	*
	*@Link https://forums.unrealengine.com/showthread.php?30209-CODE-SNIPPET-C-Detecting-if-Pawn-(or-any-actor)-is-in-light
	* 
	*/
	UFUNCTION()
	void HandleLightDetection();


	/**
	*@Description this is called when the ghost is under a spotlight in handleLight function
	*/
	UFUNCTION(BlueprintNativeEvent)
	void CalledWhenIsInLight();
	virtual void CalledWhenIsInLight_Implementation();
	
};
