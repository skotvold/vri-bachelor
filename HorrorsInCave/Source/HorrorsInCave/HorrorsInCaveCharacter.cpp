// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "HorrorsInCave.h"
#include "HorrorsInCaveCharacter.h"
#include "HorrorsInCaveProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "AI_Ghost.h"
#include "EndGoal.h"
#include "Items/item.h"
#include "Items/HealthPack.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);


AHorrorsInCaveCharacter::AHorrorsInCaveCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);


	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	GetCharacterMovement()->MaxWalkSpeed = 490;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = GetCapsuleComponent();
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(false);
	Mesh1P->AttachParent = FirstPersonCameraComponent;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);			// Seen by everyone
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->AttachTo(Mesh1P, TEXT("GripPoint"), EAttachLocation::SnapToTargetIncludingScale, true);

	// Create the collection sphere
	CollectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollectionSphere"));
	CollectionSphere->AttachTo(Mesh1P);
	CollectionSphere->SetSphereRadius(200.0f);

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 10.0f, 15.0f);

	// Set the health for the player
	iHealth = MAX_HEALTH - HEALTH_PACK_RESTORE;

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P are set in the
	// derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}


void AHorrorsInCaveCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// set up gameplay key bindings
	check(InputComponent);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Sprint
	InputComponent->BindAction("Sprint", IE_Pressed, this, &AHorrorsInCaveCharacter::Sprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AHorrorsInCaveCharacter::StopSprint);
	
	// Collect items
	InputComponent->BindAction("Collect", IE_Pressed, this, &AHorrorsInCaveCharacter::CollectItems);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AHorrorsInCaveCharacter::TouchStarted);
	if( EnableTouchscreenMovement(InputComponent) == false )
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AHorrorsInCaveCharacter::OnFire);
	}
	
	InputComponent->BindAxis("MoveForward", this, &AHorrorsInCaveCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AHorrorsInCaveCharacter::MoveRight);
	
	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AHorrorsInCaveCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AHorrorsInCaveCharacter::LookUpAtRate);
}

void AHorrorsInCaveCharacter::OnFire()
{ 
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		const FRotator SpawnRotation = GetControlRotation();
		// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
		const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);

		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			// spawn the projectile at the muzzle
			World->SpawnActor<AHorrorsInCaveProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if(FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if(AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}

}

void AHorrorsInCaveCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if( TouchItem.bIsPressed == true )
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AHorrorsInCaveCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if( ( FingerIndex == TouchItem.FingerIndex ) && (TouchItem.bMoved == false) )
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void AHorrorsInCaveCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if ((TouchItem.bIsPressed == true) && ( TouchItem.FingerIndex==FingerIndex))
	{
		if (TouchItem.bIsPressed)
		{
			if (GetWorld() != nullptr)
			{
				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
				if (ViewportClient != nullptr)
				{
					FVector MoveDelta = Location - TouchItem.Location;
					FVector2D ScreenSize;
					ViewportClient->GetViewportSize(ScreenSize);
					FVector2D ScaledDelta = FVector2D( MoveDelta.X, MoveDelta.Y) / ScreenSize;									
					if (ScaledDelta.X != 0.0f)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.X * BaseTurnRate;
						AddControllerYawInput(Value);
					}
					if (ScaledDelta.Y != 0.0f)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.Y* BaseTurnRate;
						AddControllerPitchInput(Value);
					}
					TouchItem.Location = Location;
				}
				TouchItem.Location = Location;
			}
		}
	}
}

void AHorrorsInCaveCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AHorrorsInCaveCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AHorrorsInCaveCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AHorrorsInCaveCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AHorrorsInCaveCharacter::Sprint()
{
	GetCharacterMovement()->MaxWalkSpeed += SPRINT_VALUE;
}

void AHorrorsInCaveCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed -= SPRINT_VALUE;
}

bool AHorrorsInCaveCharacter::EnableTouchscreenMovement(class UInputComponent* InputComponent)
{
	bool bResult = false;
	if(FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch )
	{
		bResult = true;
		InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AHorrorsInCaveCharacter::BeginTouch);
		InputComponent->BindTouch(EInputEvent::IE_Released, this, &AHorrorsInCaveCharacter::EndTouch);
		InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AHorrorsInCaveCharacter::TouchUpdate);
	}
	return bResult;
}


void AHorrorsInCaveCharacter::ReceiveHit(class UPrimitiveComponent *MyComp, class AActor *Other,
						class UPrimitiveComponent *OtherComp, bool bSelfMoved,
							FVector HitLocation, FVector HitNormal,
							FVector NormalImpulse, const FHitResult &Hit)
{
	if (Other->IsA(AAI_Ghost::StaticClass()))
	{
		// Take damage if the hits a ghost
		iHealth -= DAMAGE_RECEIVE;
		UE_LOG(LogTemp, Warning, TEXT("Your Health is : %f "), iHealth);
	}

	if (iHealth <= DEATH)
	{
		// Disable the controlls when dead 
		DisableInput(GetWorld()->GetFirstPlayerController());
		// Open menu after
		//-----//
		UGameplayStatics::OpenLevel(this, "/Game/HorrorsInCaves/MainMenu", false);
	}
}


void AHorrorsInCaveCharacter::CollectItems()
{
	// Get all overlapping actors and store them in an array
	TArray<AActor*> CollectedActors;
	CollectionSphere->GetOverlappingActors(CollectedActors);

	// for each actor we collected
	for (int32 iCollected = 0; iCollected < CollectedActors.Num(); ++iCollected)
	{
		// Cast the actor to Aitem
		Aitem* const TestItems = Cast<Aitem>(CollectedActors[iCollected]);

		// if the cast is successfull and the item is valid and active
		if (TestItems && !TestItems->IsPendingKill() && TestItems->isActive())
		{
			
			// call the pickup's WasCollected function
			TestItems->WasCollected();

			// if the item was a health pack
			if (TestItems->IsA(AHealthPack::StaticClass()))
			{
				// Restore health if the health is not at full capacity
				if (iHealth != MAX_HEALTH)
				{
					// if health is 9, just add 1 more health point
					if (iHealth == 9.f)
					{
						iHealth += HEALTH_PACK_RESTORE - 1.f;
					}
					// else restore 2 health points
					else
					{
						iHealth += HEALTH_PACK_RESTORE;
					}
				}
				// debug message for knowing your health
				UE_LOG(LogTemp, Warning, TEXT("Your Health is : %f "), iHealth);
			}
			// Deactive the pickup
			TestItems->SetActive(false);
		}
	}
}