// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "AI_Enemy.generated.h"

UCLASS()
class HORRORSINCAVE_API AAI_Enemy : public ACharacter
{
	GENERATED_BODY()


private:


protected:
	// Bool flag to set if Enemy is active
	bool bIsActive;
	//The amount of damage for a enemy to take, can be made higher depending on settings
	int DamageRecieved;

public:

	// Current health for player
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character)
	float iHealth;

	// Sets default values for this character's properties
	AAI_Enemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	//Return if its active or not
	UFUNCTION(BlueprintPure, Category = "Enemy")
	bool IsActive();

	//Return if its active or not
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void SetActive(bool NewActiveState);
	
};
