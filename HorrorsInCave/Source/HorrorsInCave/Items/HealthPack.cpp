// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "HealthPack.h"
#include "HorrorsInCaveCharacter.h"


// set default values

AHealthPack::AHealthPack()
{
	GetMesh()->SetSimulatePhysics(true);

}



void AHealthPack::WasCollected_Implementation()
{
	// Use the base pickup behavior
	Super::WasCollected_Implementation();
	//Destory the battery
	Destroy();
}