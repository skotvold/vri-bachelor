// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "item.h"
#include "HorrorsInCaveCharacter.h"
#include "HorrorsInCaveProjectile.h"

// Sets default values
Aitem::Aitem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	//Set the item as active 
	bIsActive = true;
	// Create the static mesh for the item
	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
	RootComponent = ItemMesh;

}

// Called when the game starts or when spawned
void Aitem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void Aitem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	// check for players every tick
	WithinReachOfPlayer();
}

bool Aitem::isActive()
{
	return bIsActive;
}

void Aitem::SetActive(bool NewActiveState)
{
	bIsActive = NewActiveState;
}


void Aitem::WasCollected_Implementation()
{
	// Log a debug message
	FString ItemDebugString = GetName();
	UE_LOG(LogClass, Log, TEXT("You have collected %s"), *ItemDebugString);
}

void Aitem::WithinReachOfPlayer()
{
	// Get all overlapping actors and store them in an array
	/*TArray<AActor*> CollectedActors;
	CollectionSphere->GetOverlappingActors(CollectedActors);

	// for each actor we collected
	for (int32 iCollected = 0; iCollected < CollectedActors.Num(); ++iCollected)
	{
		// Cast the actor to AHorrorsInCaveCharacter
		AHorrorsInCaveCharacter* const TestCharacter = Cast<AHorrorsInCaveCharacter>(CollectedActors[iCollected]);

		// if the cast is successfull
		if (TestCharacter && !TestCharacter->IsPendingKill())
		{
			// Log a debug message
			FString ItemDebugString = GetName();
			UE_LOG(LogClass, Log, TEXT("You are within range of  %s"), *ItemDebugString);
		}
	}*/
}


void Aitem::ReceiveHit(class UPrimitiveComponent *MyComp, class AActor *Other,
class UPrimitiveComponent *OtherComp, bool bSelfMoved,
	FVector HitLocation, FVector HitNormal,
	FVector NormalImpulse, const FHitResult &Hit)
{
	// logs what actor that hit the item unless it is the player that hit it
	if (!Other->IsA(AHorrorsInCaveCharacter::StaticClass()))
	{
		//UE_LOG(LogTemp, Warning, TEXT("This item got hit by the %s"), *Other->GetName());
	}
}
