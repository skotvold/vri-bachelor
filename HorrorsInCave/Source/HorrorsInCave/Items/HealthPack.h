// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/item.h"
#include "HealthPack.generated.h"

/**
 * 
 */
UCLASS()
class HORRORSINCAVE_API AHealthPack : public Aitem
{
	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	AHealthPack();

	/*Override WasCollected function - use iomplementation because its a bluepirnt native event*/
	void WasCollected_Implementation() override;
	
};
