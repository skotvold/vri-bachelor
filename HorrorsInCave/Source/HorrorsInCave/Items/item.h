// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "item.generated.h"

UCLASS()
class HORRORSINCAVE_API Aitem : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent *ItemMesh;

	/** Collection Sphere for picking up items */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item, meta = (AllowPrivateAccess = "true"))
	class USphereComponent *CollectionSphere;

	
public:	


	// Sets default values for this actor's properties
	Aitem();

	// Return the mesh for the item
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return ItemMesh; }

	//Return if the item is active or not
	UFUNCTION(BlueprintPure, Category = "Item")
	bool isActive();

	// Allows other classes to safely change if its active or not active
	UFUNCTION(BlueprintCallable, Category = "Item")
	void SetActive(bool NewActiveState);

	/*Function to call when the item is collected*/
	UFUNCTION(BlueprintNativeEvent)
	void WasCollected();
	virtual void WasCollected_Implementation();

	/**
	*@Description
	*highlight that an item can be picked up if its in reach of player
	*/
	UFUNCTION(BlueprintCallable, Category = "Item")
	void WithinReachOfPlayer();


	/**
	*log what happens when item is hit
	*
	*@param Other this is the other actor we check if it is a projectile then we see if we can destroy the enemy
	*/
	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual void ReceiveHit(class UPrimitiveComponent *MyComp, class AActor *Other,
	class UPrimitiveComponent *OtherComp, bool bSelfMoved,
		FVector HitLocation, FVector HitNormal,
		FVector NormalImpulse, const FHitResult &Hit);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
	// flag for item, if it can be used t = can be used, f = cannot be used
	bool bIsActive;
	
	/** Returns SphereCollection subobject **/
	FORCEINLINE class USphereComponent* GetCollectionSphere() const { return CollectionSphere; }

};
