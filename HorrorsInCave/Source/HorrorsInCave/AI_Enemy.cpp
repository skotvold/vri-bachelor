// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "AI_Enemy.h"


// Sets default values
AAI_Enemy::AAI_Enemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Sets the enemies to 1
	iHealth = 1;

	// Sets the damage to be recieved when hit to 1
	DamageRecieved = 1.f;

}

// Called when the game starts or when spawned
void AAI_Enemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Enemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AAI_Enemy::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

/**
*Get current state if it is active or not
*
*@return Return true if enemy is active, false if it is not active
*/
bool AAI_Enemy::IsActive()
{
	return bIsActive;
}

/**
*Set the active state
*
*@param NewActiveState Set t for active and f for not active
*/
void AAI_Enemy::SetActive(bool NewActiveState)
{
	bIsActive = NewActiveState;
}


