// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "AEnemy.generated.h"

UCLASS()
class HORRORSINCAVE_API AAEnemy : public AActor
{
	GENERATED_BODY()
private:

	/* Static mesh to represent the enemy in the level*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemy", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *EnemyMesh;
	
protected:
	// Bool flag to set if Enemy is active
	bool bIsActive;

	// Health of the enemy
	int iHealth;

public:	
	// Sets default values for this actor's properties
	AAEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Return if its active or not
	UFUNCTION(BlueprintPure, Category = "Enemy")
	bool IsActive();

	//Return if its active or not
	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void SetActive(bool NewActiveState);

	// Return the mesh for the pickup
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return EnemyMesh; }

	
};