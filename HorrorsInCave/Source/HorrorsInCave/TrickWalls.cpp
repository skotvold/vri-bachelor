// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "TrickWalls.h"
#include "Drone.h"

// Sets default values
ATrickWalls::ATrickWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Replicate this object
	bReplicates = true;
	bIsUnderLight = false;

	// Create the mesh for the wall
	WallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallMesh"));
	WallMesh->AttachTo(RootComponent);

	// create a box component for detection of light
	LightDetectionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("LightDetection"));
	LightDetectionComponent->AttachTo(WallMesh);
}

// Called when the game starts or when spawned
void ATrickWalls::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATrickWalls::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	// Check if the object is in the light every tick
	HandleLightDetection();
}

void ATrickWalls::HandleLightDetection()
{
	// set bIsUnderLight as false to reset the bool
	bIsUnderLight = false;

	for (TObjectIterator<USpotLightComponent> Itr; Itr; ++Itr)
	{
		// If this light is enabled (or visible),
		if (Itr->IsVisible())
		{
			// and if the light affects this actor mesh component,
			if (Itr->AffectsPrimitive(LightDetectionComponent))
			{
				// and if the light is a part of the ADrone class
				if (Itr->IsInA(ADrone::StaticClass()))
				{
					// Then set bIsUnderLight to true
					bIsUnderLight = true;
				}
			}
		}
	}

	// If the ghost is under a SpotLight 
	if (bIsUnderLight)
	{
		// Hide the Object and disable the collision
		WallMesh->SetHiddenInGame(true);
		WallMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		return;
	}
	else
	{
		// else set the wall as visible and enable collision
		WallMesh->SetHiddenInGame(false);
		WallMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}