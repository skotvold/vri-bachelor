// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "AI_Ghost.h"
#include "HorrorsInCaveProjectile.h"
#include "HorrorsInCaveCharacter.h"
#include "Drone.h"




AAI_Ghost::AAI_Ghost()
{
	//The the bIsStunned to false since it wont be stunned when spawned
	bIsStunned = false;
	bIsUnderLight = false;
	bReplicates = true;

	// Set the health for ghost
	iHealth = MAX_HEALTH;
	// Set the movementspeed to 280
	GetCharacterMovement()->MaxWalkSpeed = 280;
}


// Called every frame
void AAI_Ghost::Tick(float DeltaSeconds)
{
	HandleLightDetection();
}




void AAI_Ghost::SetStunned(bool NewStunnedState)
{
	bIsStunned = NewStunnedState;
}

bool AAI_Ghost::IsStunned()
{
	return bIsStunned;
}

void AAI_Ghost::ReceiveHit(class UPrimitiveComponent *MyComp, class AActor *Other,
							class UPrimitiveComponent *OtherComp, bool bSelfMoved,
								FVector HitLocation, FVector HitNormal,
								FVector NormalImpulse, const FHitResult &Hit)
{
	// logs what actor that hit the Ghost unless it is the player that hit it
	if (!Other->IsA(AHorrorsInCaveCharacter::StaticClass()))
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit by the %s"), *Other->GetName());
	}

	// Destroy the Ghost if it is stunned and active
	if (Other->IsA(AHorrorsInCaveProjectile::StaticClass()))
	{
		if (bIsStunned == true)
		{
			// Reduce the amount of health on ghost
			iHealth -= DamageRecieved;
			//if the ghost is dead
			if (iHealth == 0.0f)
			{
				// set the enemy as inactive and destory the ghost
				SetActive(false);
				Destroy();
			}
		}
	}
}

void AAI_Ghost::HandleLightDetection()
{

	// set bIsUnderLight as false to reset the bool
	bIsUnderLight = false;

	for (TObjectIterator<USpotLightComponent> Itr; Itr; ++Itr)
	{
		// If this light is enabled (or visible),
		if (Itr->IsVisible())
		{
			// and if the light affects this character's capsule component,
			if (Itr->AffectsPrimitive(GetCapsuleComponent()))
			{
				// and if the light is a part of the ADrone class
				if (Itr->IsInA(ADrone::StaticClass()))
				{
					// Then set bIsUnderLight to true
					bIsUnderLight = true;
				}
			}
		}
	}

	// If the ghost is under a SportLight set bIsStunned to true
	if (bIsUnderLight)
	{
		// made a blueprintNativeEvent to make a particle effect appear when it is underthelight
		CalledWhenIsInLight();
		bIsStunned = true;
		return;
	}
	else
	{
		// Otherwise set bIsStunned to false
		//UE_LOG(LogTemp, Warning, TEXT("This ghost is not stunned"));
		bIsStunned = false;
	}
}


void AAI_Ghost::CalledWhenIsInLight_Implementation()
{
	// This exist exclusivly for use in Blueprints
}