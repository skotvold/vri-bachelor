/**
*@file
*@author Jan-Henrik Thorsen <janhenrikth@gmail.com>
*
*@section DESCRIPTION
*
*The AAEnemy class represent a enemy within the game
*/

#include "HorrorsInCave.h"
#include "AEnemy.h"

// Sets default values
AAEnemy::AAEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Sets the enemies to 1
	iHealth = 1;

	// Create the static mesh component
	EnemyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Enemy Mesh"));
	RootComponent = EnemyMesh;

	// Set physics for the mesh
	GetMesh()->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void AAEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


/**
*Get current state if it is active or not
*
*@return Return true if enemy is active, false if it is not active
*/
bool AAEnemy::IsActive()
{
	return bIsActive;
}

/**
*Set the active state
*
*@param NewActiveState Set t for active and f for not active
*/
void AAEnemy::SetActive(bool NewActiveState)
{
	bIsActive = NewActiveState;
}

