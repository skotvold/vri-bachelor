// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "HorrorsInCave.h"
#include "HorrorsInCaveGameMode.h"
#include "HorrorsInCaveHUD.h"
#include "HorrorsInCaveCharacter.h"
#include "HorrorsInCaves_GameState.h"
#include "Drone.h"
#include "Engine.h"




AHorrorsInCaveGameMode::AHorrorsInCaveGameMode() : Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	static ConstructorHelpers::FClassFinder<APawn> SecondPlayerPawnClassFinder(TEXT("/Game/HorrorsInCaves/Blueprints/Drone_BP"));

	// Initialize the gamestate class
	GameStateClass = AHorrorsInCaves_GameState::StaticClass();

	// Initialize the type of pawns/players
	ShooterPawnClass =	PlayerPawnClassFinder.Class;
	DronePawnClass	 =	SecondPlayerPawnClassFinder.Class;
	
	// Initialize and set players spawn position at zero
	SpawnPositionLocalPlayer = FVector(0.0f, 0.0f, 0.0f);
	SpawnPositionClientPlayer = FVector(0.0f, 0.0f, 0.0f);

}


void AHorrorsInCaveGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	APlayerController* controller = GetPC();

	for (auto Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{

		// assign each player with their character
		auto num = GetWorld()->GetNumLevels();
		if (!IsLocalPlayer(Iterator->Get()) && NumPlayers > 1)
		{
			// Set the players characters and spawn position
			ChangePawn(controller, ShooterPawnClass, SpawnPositionLocalPlayer);
			ChangePawn(NewPlayer, DronePawnClass, SpawnPositionClientPlayer);
		
			// Make sure we don't move when both players are not in the game.
			// But can move when players spawn have established an connection
			// and the world have more than 1 player
		
			if (NumPlayers > 1)
			{
				Iterator->Get()->ResetIgnoreMoveInput();
			}
		}
	}
}



bool AHorrorsInCaveGameMode::IsLocalPlayer(APlayerController* Controller)
{

	// check if the controller is local players controller
	// On server local controller is the server,
	if (Controller)
	{
		return GEngine->GetGamePlayer(GetWorld(), 0)->PlayerController == Cast<APlayerController>(Controller);
	}

	return false;
}


APlayerController* AHorrorsInCaveGameMode::GetPC() {

	//Not looping, just want the first entry
	TObjectIterator<APlayerController> Itr; 
	
	if (!Itr)
	{
		// This can happen while PIE is exiting ( the editor )
		return nullptr;				
	}
	

	// return the Itr code
	return *Itr;
}


void AHorrorsInCaveGameMode::ChangePawn(APlayerController* PlayerPawn , TSubclassOf<APawn> TypeOfPawn)
{

	// Dummy pawn to store the already possed pawn
	// unposses the pawn from the "player controller"
	
	APawn* dummy = PlayerPawn->GetPawn();
	PlayerPawn->UnPossess();

	// Kill/Destroy the dummy pawn if it exist, remove the mesh
	if (!dummy->IsPendingKill() && dummy != nullptr)
	{
		dummy->Destroy();
	}

	
	// Spawn and Attach a new pawn to the player controller
	auto NewPawn = GetWorld()->SpawnActor<APawn>(TypeOfPawn, this->ChoosePlayerStart(PlayerPawn)->GetActorLocation(), FRotator(0.f, 0.f, 0.f));
	PlayerPawn->Possess(NewPawn);
}


void AHorrorsInCaveGameMode::ChangePawn(APlayerController* PlayerPawn, TSubclassOf<APawn> TypeOfPawn, FVector spawn)
{

	// Dummy pawn to store the already possed pawn
	// unposses the pawn from the "player controller"

	APawn* dummy = PlayerPawn->GetPawn();
	PlayerPawn->UnPossess();

	// Kill/Destroy the dummy pawn if it exist, remove the mesh
	if (!dummy->IsPendingKill() && dummy != nullptr)
	{
		dummy->Destroy();
	}


	// Spawn and Attach a new pawn to the player controller
	auto NewPawn = GetWorld()->SpawnActor<APawn>(TypeOfPawn, spawn, FRotator(0.f, 0.f, 0.f));
	PlayerPawn->Possess(NewPawn);
}




void AHorrorsInCaveGameMode::BeginPlay()
{

	// Set the player to not be able to move around at the start of game play
	// For menu control and at startup
	APlayerController* controller = GetPC();
	controller->SetIgnoreMoveInput(true);
	SetSpawnPositions(GetWorld()->GetMapName());
}

void AHorrorsInCaveGameMode::SetSpawnPositions(FString ID)
{

	if (ID == "UEDPC")
	{
		ID = GetWorld()->StreamingLevelsPrefix;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Could not find spawnlocations problems"));

	}

	// Set spawn position on labyrinth
	if (ID == "Labyrinth" || ID == "UEDPCLabyrinth" || ID == "UEDPIE_1_Labyrinth")
	{
		SpawnPositionLocalPlayer  = FVector(1668.32f, -936.164f, 178.149f);
		SpawnPositionClientPlayer = FVector(-2759.529f, 3610.0f, 826.0f);

	}

	// Set spawn positions on HorrosInCaves1 (small level)
	else if (ID == "HorrorsInCaves" || ID == "UEDPCHorrosInCaves" || ID == "UEDPIE_1_HorrorsInCaves")
	{
		SpawnPositionLocalPlayer  = FVector(4823.0815f, -720.518f, 3657.779f);
		SpawnPositionClientPlayer = FVector(7153.0f, 4069.334f, 4827.489f);

	}


	// Set spawn positions on HorrosInCaves2 ( big level
	else if (ID == "HorrorsInCaves2" || ID == "UEDPCHorrosInCaves2" || ID == "UEDPIE_1_HorrorsInCaves2")
	{
	
		SpawnPositionLocalPlayer  = FVector(-511.06f, -238.09f, 157.02f);
		SpawnPositionClientPlayer = FVector(-211.06f, -498.09f, 158.00f);

	}


	// Level name wasn not found!
	else
	{
		// If map name wasn't found spawn at default spawn location
		// This should not happen, better safe than sorry
		SpawnPositionLocalPlayer = FVector::ZeroVector;
		SpawnPositionClientPlayer = FVector::ZeroVector;
	}

}

