// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "Drone.h"
#include "Engine.h"


// Sets default values
ADrone::ADrone()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Turn rates for input
	BaseTurnRate = 45.0f;
	BaseLookUpRate = 45.0f;

	//Make sure the actor replicates
	bReplicates = true;

	// Speed at which to decrease
	SpeedDecrease = -0.001f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = FFloat16(0.0f);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a CameraComponent
	ThirdPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	ThirdPersonCameraComponent->bUsePawnControlRotation = false;

	// Create a mesh for the drone views in 3rd person
	DroneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DroneMesh"));
	DroneMesh->SetOnlyOwnerSee(false);
	DroneMesh->AttachTo(GetCapsuleComponent());

}

// Called when the game starts or when spawned
void ADrone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADrone::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
	Decrease();
}

// Called to bind functionality to input
void ADrone::SetupPlayerInputComponent(class UInputComponent *InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	// Set up gameplay key bindings
	check(InputComponent);
	
	InputComponent->BindAxis("Ascend", this, &ADrone::ServerAscend);
	InputComponent->BindAxis("MoveForward", this, &ADrone::MoveForwardAndBack);
	InputComponent->BindAxis("MoveRight", this, &ADrone::MoveRightAndLeft);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ADrone::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ADrone::LookUpAtRate);
}

void ADrone::MoveForwardAndBack(float value)
{
	if (value != 0.0f)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//Get front vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		

		//UE_LOG(LogTemp, Warning, TEXT("the direction is %s"), *Direction.ToString());

		// Add movement in that direction
		AddMovementInput(Direction, value);

	}

}

void ADrone::MoveRightAndLeft(float value)
{
	if ((Controller != NULL) && (value != 0.0f))
	{
		
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// Got Right vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		//Add movement in that direction
		AddMovementInput(Direction, value);
	}
}



void ADrone::Ascend(float value)
{

	
	// checks if it is a valid value
	if ((value != 0.0f) && (Controller != NULL))
	{

		// Make a vector in Z direction and set that as the new Actor Location
		CurrentVelocity.Z = FMath::Clamp(value, -1.0f, 1.0f) * 5.0f;
		FVector NewLocation = GetActorLocation() + (CurrentVelocity);
		SetActorLocation(NewLocation);
	
	}
}

void ADrone::ServerAscend_Implementation(float value)
{

	// checks if it is a valid value
	if ((value != 0.0f) && (Controller != NULL))
	{

		// Make a vector in Z direction and set that as the new Actor Location
		CurrentVelocity.Z = FMath::Clamp(value, -1.0f, 1.0f) * 5.0f;
		FVector NewLocation = GetActorLocation() + (CurrentVelocity);
		SetActorLocation(NewLocation);

	}
}

bool ADrone::ServerAscend_Validate(float value)
{
	return true;
}

void ADrone::TurnAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}


void ADrone::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}


void ADrone::Decrease()
{
	AddMovementInput(GetVelocity(), SpeedDecrease);
}