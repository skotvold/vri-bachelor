/** @brief HorrorsInCave Game Mode class

*   @section Description
*
*	Game Mode class controlles the flow of the game Horrors In Cave.
*	Specially the handles most of the games Server code and the connection
*	between the players.

*	@date 2016
*   @section LICENSCE 
*
*	Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
*/

#pragma once

#include "GameFramework/GameMode.h"
#include "HorrorsInCaveGameMode.generated.h"


UCLASS()
class  AHorrorsInCaveGameMode : public AGameMode
{
	GENERATED_BODY()


	
public:
	
	/** Construct and Create the GameMode class.*/
	AHorrorsInCaveGameMode();
		
	/** Handle the connection and assignment of the players */
	virtual void PostLogin(APlayerController* NewPlayer) override;

	/** Check if a Player Controller is the local player or an client player
	 * 
	 * @return A bool object set to true or false dependent if the player is the local or not.
	 */
	bool IsLocalPlayer(APlayerController* Controller);
	
	/**
	 * Get the local or the servers player controller. 
	 * Clients will fetch their own controllers. 
	 * This helps us find if the player is a controller or not. 
	 *
	 * @return A AplayerController pointer to the server controller. 
	 */
	FORCEINLINE APlayerController* GetPC();

   /**
	* Change pawn class for player
	* Assign a controller to APawn class
	* Removes/delete already possesd pawns
	*
	* @param PlayerPawn APlayerController object
	* @param TypeOfPawn Type of pawn(either a VR-Player or NON-VR Player
	*/
	void ChangePawn(APlayerController* PlayerPawn, TSubclassOf<APawn> TypeOfPawn);
	
	/**
	* Change pawn class for player
	* Assign a controller to APawn class
	* Removes/delete already possesd pawns
	*
	* @param PlayerPawn APlayerController object
	* @param TypeOfPawn Type of pawn(either a VR-Player or NON-VR Player
	* @param spawn Vector contains World coordinate where player should be spawning. 
	*/
	void ChangePawn(APlayerController* PlayerPawn, TSubclassOf<APawn> TypeOfPawn, FVector spawn);

	/** Set the start requirments when the game starts */
	virtual void BeginPlay() override;
	
   /** Store our drone class */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameMode)
	TSubclassOf<APawn> DronePawnClass;

	/** Store our Shooter Player class */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameMode)
	TSubclassOf<APawn> ShooterPawnClass;

	/** Spawn the position of the server player or the VR-Player */
	FVector SpawnPositionLocalPlayer;

	/** Spawn the position of the client Player or the NON-VR player*/
	FVector SpawnPositionClientPlayer;

	/** 
	 * Set the spawn postion dependant on the map/level 
	 *
	 * @param ID A string object set to current map/level
	 */
	void SetSpawnPositions(FString ID);

	
};

