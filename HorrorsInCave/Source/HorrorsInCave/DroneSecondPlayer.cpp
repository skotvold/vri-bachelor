// Fill out your copyright notice in the Description page of Project Settings.

#include "HorrorsInCave.h"
#include "DroneSecondPlayer.h"


// Sets default values
ADroneSecondPlayer::ADroneSecondPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADroneSecondPlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADroneSecondPlayer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ADroneSecondPlayer::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

