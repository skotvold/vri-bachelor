// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Drone.generated.h"

UCLASS()
class HORRORSINCAVE_API ADrone : public ACharacter
{
	GENERATED_BODY()

private:

	/*Light under the drone to stun the enemies*/
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//class USpotLightComponent *DroneLight;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UStaticMeshComponent *DroneMesh;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* ThirdPersonCameraComponent;

public:
	// Sets default values for this character's properties
	ADrone();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return ThirdPersonCameraComponent; }

	// The Decrease speed
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Speed)
	float SpeedDecrease;

	FVector CurrentVelocity;

protected:

	// Move the drone forward and backward
	UFUNCTION()
	void MoveForwardAndBack(float value);

	//Move the drone right and left
	UFUNCTION()
	void MoveRightAndLeft(float value);

	// Called via input to turn at a given rate
	UFUNCTION()
	void TurnAtRate(float Rate);
   
   /**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/**
	*@Description Ascend or Descend the drone depening on keystroke
	*
	*@param value is set in the project settings along with a corresponding key and value
	*/
	// Ascend the drone Locally
	UFUNCTION()
	void Ascend(float value);

	
	 /** ServerAscend
	 * @Param value
	 * Function is called on client, but executed on server
	 */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAscend(float value);
	void ServerAscend_Implementation(float value);
	bool ServerAscend_Validate(float value);

	/**
	*Decrease the speed until the drone stops to avoid gliding forever
	*/
	UFUNCTION()
	void Decrease();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
};