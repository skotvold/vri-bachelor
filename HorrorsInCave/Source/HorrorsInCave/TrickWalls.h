// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TrickWalls.generated.h"

UCLASS()
class HORRORSINCAVE_API ATrickWalls : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class UStaticMeshComponent *WallMesh;

	/*a mesh for light detection that is in the centre of the wall mesh, to avoid the wall being so easily detected by drone light*/
	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class UBoxComponent *LightDetectionComponent;

private:
	// name to control if the light that hits this wall has a tag with this name
	const FName LightDetectionControl = "LightDetection";

public:	

	// Flag to see if the wall is in light
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsUnderLight;


	// Sets default values for this actor's properties
	ATrickWalls();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Handle its Light Detection
	void HandleLightDetection();
	
};
