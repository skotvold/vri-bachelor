# Professional Programming IMT3602 2016
This file is used for the professional programming course IMT 3602. It include discussion and reflections from what I have learned in the course. I will use the bachelor code as examples of what is bad code and what is good code from the project. I will also link to other code examples taken from external repositories, or from code guides on the web to show examples and make my explanations easier to understand.

The three most important branches from the VRI-bachelor project are:

* [master branch](https://bitbucket.org/skotvold/vri-bachelor/src)
* [Professional-Programming branch](https://bitbucket.org/skotvold/vri-bachelor/src/a1309b982f74ffa8191d1dd7ada5cc3111545e11/?at=Professional-Programming)
* [game_mode](https://bitbucket.org/skotvold/vri-bachelor/src)

Less important, but can be good for looking at the commit logs. 

* [development](https://bitbucket.org/skotvold/vri-bachelor/src)

The branches did not follow a specific naming pattern, which it probably should have. We should have set those as rules in the git conventions. Can be found at the confluence page under the appendix - [Git Conventions ](https://dev.imt.hig.no/confluence/display/VRI/Git+conventions)

I(Eskil André Skotvold) was the only attendance from our group in professional programming course. So all code was not written by me, but I have done reflections on how the group could have been better and more professional programmers as a group in certain areas. Since some of these topics and the code felt relevant to the course. 
These topics are:

* Name for variables and classes.
* Documentation.
* Using similar code approaches for problems that use the same pattern.

## Pre-bachelor
Before we started developing and coding on the bachelor project. We made some coding conventions that can be found at the conflunece page about our bachelor under the appendix - [coding conventions](https://dev.imt.hig.no/confluence/display/VRI/Coding+conventions). The coding conventions was meant as a set of guidelines to help us be better sync when developing code. It includes doxygen documenting(which was not done) and examples how our code style could be used to look as similar to unreal engine 4's standard. 

## Code from the project
There are a lot of code in this project that could have been improved. But there are also some code that could probably survive. 
### documentation
Example of good usage of doxygen commenting style [here ](https://bitbucket.org/skotvold/vri-bachelor/src/75c644b4c75a1f68aa86c754efc596f200b8bbee/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveGameMode.h?at=master&fileviewer=file-view-default) from the HorrorsInCaveGameMode.h file in professional programming branch. I have left the master and other branch be untouched, they did not use the doxygen commenting style. 

I think this is a good way of doing it. Doxygen is a great tool for creating documents of your classes that makes it easy to get a good overview of the class functions and members. A minor negative effect is the readability. Myself like the header files to not be as compact and less commented as possible. This make it easy to get quick looks and understand the what the class are doing. But professionally I also prefer more commenting thant less. Which is why I like the doxygen style, since I can use the html or generated pdf to get the short overviews, but also detailed explanation of what the code is doing.

During the project there was multiple different way of documenting, some header files used a doxygen pattern. Some did not use a Doxygen pattern at all. So we was not consistence enough. 

Example Drone.h used a bit of both patterns: [Drone.h ](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/Drone.h?at=master&fileviewer=file-view-default). I decided not to change any code or documentation, except codes given as examples this is how it should have been done (like the HorrorsInCaveGameMode.h file example given above. I decided it was better to show that I can identify problems and show that my learning from the professional programming course could be used to improve the code and make it better. 

### Naming of the Variables and classes
Our task had two types of players or actors(UE4's term). It was a VR-Player and a NON-VR player. The naming of these classes was bad. Example the drone class changed from drone to ghost, then from ghost to spirit etc etc.
The best option here would be to use a generic name in the beginning like: class NonVRCharacter and VRCharacter. Now they are generic and independent and cane be overrided for future use. Even change of type would not matter. The naming also explain very well what the class exist for. 

In the UE4 standard way doing class naming you use: A<NameOfProject><Purpose>, Example: AHorrorsInCaveGameMode. This was done on the classes that was genrated from the UE4 engine, but the classes we choose to generate as extra and ourself did not use this naming on the file names. I believe they used this naming in the code itself: [Source](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c/HorrorsInCave/Source/HorrorsInCave/?at=master)


### Using similar code approaches for problems that use the same pattern
The two cpp files we are going to look at was written by both groupmembers. The approach for iteration is different, this probably should have a baseline/standard way discussed in the group for it be more professional code. The two files are:

* [HorrorsInCavesGameMode.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveGameMode.cpp?at=master&fileviewer=file-view-default)
* [AI_Ghost.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/AI_Ghost.cpp?at=master&fileviewer=file-view-default)
 
The functions we are looking for are 

GameMode::PostLogin() 
```C++
	Super::PostLogin(NewPlayer);
	APlayerController* controller = GetPC();

	for (auto Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		auto num = GetWorld()->GetNumLevels();
		if (!IsLocalPlayer(Iterator->Get()) && NumPlayers > 1)
		{
			// Set the players characters and spawn position
			ChangePawn(controller, ShooterPawnClass, SpawnPositionLocalPlayer);
			ChangePawn(NewPlayer, DronePawnClass, SpawnPositionClientPlayer);
		}
	}
}
```
and AI_Ghost::HandleLightDetection()
```C++
	// set bIsUnderLight as false to reset the bool
	bIsUnderLight = false;

	for (TObjectIterator<USpotLightComponent> Itr; Itr; ++Itr)
	{
		// If this light is enabled (or visible),
		if (Itr->IsVisible())
		{
			// and if the light affects this character's capsule component,
			if (Itr->AffectsPrimitive(GetCapsuleComponent()))
			{
				// and if the light is a part of the ADrone class
				if (Itr->IsInA(ADrone::StaticClass()))
				{
					// Then set bIsUnderLight to true
					bIsUnderLight = true;
				}
			}
		}
	}
```

The approach is the same, except the first code use auto and a Get-function from the UE4. The second approach define a object iterator of a type given what you want to iterate. Both method does the same, iterate through given objects in a list. I have done some research on the UE4 forums, and both method seem to be used. So both methods are fine for use of iterating through objects in your game. Problem is that both method should have used the same pattern. As far I know there may not exist a GetSpotLightComponents in the game world as a function. Which means take all objects in the world and only iterate through those of type USpotLightComponent would be correct decision. So the Light detection code may have to check/iterate through more objects than the auto getPlayerControllers() method. So in theory the auto method should be changed:

```C++
	Super::PostLogin(NewPlayer);
	APlayerController* controller = GetPC();

	for (TObjectIterator<APlayerController> Itr; Iterator; ++Iterator)
	{
		auto num = GetWorld()->GetNumLevels();
		if (!IsLocalPlayer(Iterator->Get()) && NumPlayers > 1)
		{
			// Set the players characters and spawn position
			ChangePawn(controller, ShooterPawnClass, SpawnPositionLocalPlayer);
			ChangePawn(NewPlayer, DronePawnClass, SpawnPositionClientPlayer);
		}
	}
}
```

This is not a problem in this project. But Imagine a bigger project, example the Vulkan API have very strict ways of doing  things. Like all members need to be initalized as structs. I still have not seen a vulkan object as instances, devices be initialized without structs. Example is given below from my own [code on github](https://github.com/Skotvold/Vulkan_examples/blob/master/tutorial_4/Renderer.cpp). Still I see this pattern all the other places I usually check. 
```C++
	// Create the instance info structure
	VkInstanceCreateInfo instance_Create_Info {};
	instance_Create_Info.sType						=	VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_Create_Info.pApplicationInfo			=	&application_info;
	instance_Create_Info.enabledLayerCount			=	_instance_layers.size();
	instance_Create_Info.ppEnabledLayerNames		=	_instance_layers.data();
	instance_Create_Info.enabledExtensionCount		=	_instance_extensions.size();
	instance_Create_Info.ppEnabledExtensionNames	=	_instance_extensions.data();
	instance_Create_Info.pNext						=	&_debug_callback_create_info;
```

There are probably multiple places in VRI where the group members have not been consistence with their code. And used different approaches for achieving the same result.

## Good Code from the project
It is hard to find a specific file that contains good and professional code, for it seems most of our files have some professional code and some bad code. 

* [HorrorsInCavesGameMode.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveGameMode.cpp?at=master&fileviewer=file-view-default) 
* [HorrorsInCavesGameMode.h](https://bitbucket.org/skotvold/vri-bachelor/src/75c644b4c75a1f68aa86c754efc596f200b8bbee/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveGameMode.h?at=Professional-Programming&fileviewer=file-view-default)

These two files have good code and bad code. The header file are well documented after I changed it to doxygen documentation. And the documentation in the cpp file are well done. So the documentation I feel is almost there when ti comes to professional standard. I find all the functions in the .cpp file to be good except the SetSpawnPosition()

void AHorrorsInCaveGameMode::SetSpawnPositions(FString ID)
```C++
void AHorrorsInCaveGameMode::SetSpawnPositions(FString ID)
{

	if (ID == "UEDPC")
	{
		ID = GetWorld()->StreamingLevelsPrefix;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Could not find spawnlocations problems"));

	}

	// Set spawn position on labyrinth
	if (ID == "Labyrinth" || ID == "UEDPCLabyrinth" || ID == "UEDPIE_1_Labyrinth")
	{
		SpawnPositionLocalPlayer  = FVector(1668.32f, -936.164f, 178.149f);
		SpawnPositionClientPlayer = FVector(-2759.529f, 3610.0f, 826.0f);

	}

	// Set spawn positions on HorrosInCaves1 (small level)
	else if (ID == "HorrorsInCaves" || ID == "UEDPCHorrosInCaves" || ID == "UEDPIE_1_HorrorsInCaves")
	{
		SpawnPositionLocalPlayer  = FVector(4823.0815f, -720.518f, 3657.779f);
		SpawnPositionClientPlayer = FVector(7153.0f, 4069.334f, 4827.489f);

	}


	// Set spawn positions on HorrosInCaves2 ( big level
	else if (ID == "HorrorsInCaves2" || ID == "UEDPCHorrosInCaves2" || ID == "UEDPIE_1_HorrorsInCaves2")
	{
	
		SpawnPositionLocalPlayer  = FVector(-511.06f, -238.09f, 157.02f);
		SpawnPositionClientPlayer = FVector(-211.06f, -498.09f, 158.00f);

	}


	// Level name wasn not found!
	else
	{
		// If map name wasn't found spawn at default spawn location
		// This should not happen, better safe than sorry
		SpawnPositionLocalPlayer = FVector::ZeroVector;
		SpawnPositionClientPlayer = FVector::ZeroVector;
	}

}
```
So this code was written for spawning the players in the correct spawn locations dependent on which level/map the game loaded. Just reading this code makes me sad. We did not find a way to get the PlayerSpawn objects in UE4, they usually are set in the editor. Of course this should be possible in code, most UE4 forum posts about this matter suggested writing your own spawn system. So I did that, just hacked together some simple code. 

The code look for string IDs from the maps, since UE4 set string name for the levels / map it load from the content folder. In a normal exported build, we are looking for the normal string name: Example: Labyrinth. This will not work when we run from the editor then it will have the name "UEDPIE_1_Labyrinth" and "UEDPCLabyrinth" if it was run from editor as standalone. You already can see the problem and how bad this solution was. I planned to remove the "UEDPIE_1_Labyrinth" and "UEDPCLabyrinth" when we was going to build the project, but decided to keep it in the code for this course. Since it is a good example of hacked together code for just achieving a simple solution at the end of the project. 

I also think the code from 

* [Drone.h](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/Drone.h?at=master&fileviewer=file-view-default)
* [Drone.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/Drone.cpp?at=master&fileviewer=file-view-default)
* [HorrorsInCaveCharacter.h](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveCharacter.h?at=master&fileviewer=file-view-default)
* [HorrorsInCaveCharacter.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/HorrorsInCaveCharacter.cpp?at=master&fileviewer=file-view-default)

have decent code that follows UE4 standards well, and are good code. 

## Story of the NON-VR player and server/client issue. 
This may not seem like a professional programming problem, but it was a problem, and I think the code got a lot better after the changes that was made after this problem occured.

The code can be found in the:

* [Drone.h](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/Drone.h?at=master&fileviewer=file-view-default)
* [Drone.cpp](https://bitbucket.org/skotvold/vri-bachelor/src/e61f10e3074c522ac20580c52b3009783798320a/HorrorsInCave/Source/HorrorsInCave/Drone.cpp?at=master&fileviewer=file-view-default)


When I was testing the drone/spirit/ghost/The Non-VR player... (-.-) with server/client solution.
I quickly found a problem with the ascend and descend functions. The object/player would just stay and move small frames up and down, and clearly could not move down or up. The Non-VR player is a flying object in 3D by the way. Of course I guess you already has figured out that the problem was that the NON-VR Player only moved on the client side, and the server repositioned the drone each time it just moved a single pixel up or down. Since the server has the autorith and copy of the GameMode as the ruler of the game, he can do this. But moving normally with forcevectors forward,backward,right and left worked smothly. Probably since UE4 replicateds physics between server and client when it comes to movement automatically.

This was the code that did not work on the server/client

Drone.h
```C++
UFUNCTION()
	void Ascend(float value);
```	

Drone.cpp
```C++
void ADrone::Ascend(float value)
{
	// checks if it is a valid value
	if ((value != 0.0f) && (Controller != NULL))
	{
		// Make a vector in Z direction and set that as the new Actor Location
		CurrentVelocity.Z = FMath::Clamp(value, -1.0f, 1.0f) * 5.0f;
		FVector NewLocation = GetActorLocation() + (CurrentVelocity);
		SetActorLocation(NewLocation);
	}
}
```	

The normal movement like left and right use forcevectors to move, as we see on the example above it is hacked together a solution to force to move location. Note the forcevector did not work with ascend. And probably since it is a flying object makes it more complex to get correct. This solution however works great locally, just not when there exist a server. SetActorLocation is clearly not replicated to the server. The solution became the RHC system UE4 uses between client and servers.

Drone.h
```C++
 /** ServerAscend
	 * @Param value
	 * Function is called on client, but executed on server
	 */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAscend(float value);
	void ServerAscend_Implementation(float value);
	bool ServerAscend_Validate(float value);
```	

Here we declare 3 functions. Where we tell Unreal engine that want the function executed on the server, it should be reliable, and we want validation. Reliable means that it has to wait on the packages, you want functions that executes often but not of importance of the game to be unreliable, meanwhile functions of importance to be reliable. 

Drone.cpp
```C++
void ADrone::ServerAscend_Implementation(float value)
{
	// checks if it is a valid value
	if ((value != 0.0f) && (Controller != NULL))
	{
		// Make a vector in Z direction and set that as the new Actor Location
		CurrentVelocity.Z = FMath::Clamp(value, -1.0f, 1.0f) * 5.0f;
		FVector NewLocation = GetActorLocation() + (CurrentVelocity);
		SetActorLocation(NewLocation);
	}
}

bool ADrone::ServerAscend_Validate(float value)
{
	return true;
}
```	

As you can see the code are exactly the same, but in the header file we made it so that it only can be executed on the server. So the code is the same, but the server is the one that executes the function. The validate was necessary for syntax purposes, or the debugger don't like it when you drop the validation. Hopefully this is going to be changed in the future release for Unreal engine 4.

To call the implementation, just call for 

ServerAscend(float value);

This code also works when not using server/client since then you are the "server" so it will execute as a normal function. So the result of refactor was a good one, the code got better, and we learned about a new tool for unreal engine 4 networking, I love this tool by the way. It makes programming networking in Ue4 a lot easier.

## What should have been done differently
Two major things that should have been done differently are:

* Documentation
* Naming of variables and objects

Of course there are lot of improvements that could have been done in the code too. But the documentation, naming of variables and objects, and even naming of the source files still frustrates me, and should be main things to fix asap to get more professional code base. 

So I have learned a lot that documentation, and naming are very important for the code to be looked at as proffesional. I decided to check on github, what code repositories I preferred when I was learning vulkan, and figured out that all of the code I have encountered are those that are well documented and the naming of the variables make sense. It does not mean they was writing better code than the others, it may happen it would be worse, but the code was understandable, and just looked more professionally. For myself I have experienced that I create better code when I do good naming and documentation myself, maybe because I think more around what the code are meant to be used for, and the generally think more about the code and layout of the project. And looking at the bachelor project, the naming and documentation are what frustrates me every time. It may have bunch of bad code, but documentation and the naming keep bugging me when I read the code. 

When it comes to the quality of the code, I believe it could been done a lot better. But the code follow Unreal engine 4 examples and their guidelines, so I think that is positive to take with me as a programmer. Using the tools how the creators of the tool intended always feel professional. 